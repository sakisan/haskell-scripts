module Tree where

import Data.Maybe
import Data.String.Utils

data Tree
    = Leaf String
    | Branch String
             [Tree]
    deriving (Show, Eq)

testTree :: Tree
testTree =
    Branch
        "root"
        [ Branch "house" [Leaf "kitchen", Leaf "hall", Leaf "living room"]
        , Leaf "lawn"
        , Branch "empty" []
        , Leaf "driveway"
        ]

parseTree :: String -> Tree
parseTree = joinTrees . mergeTrees . fmap parseSingle . lines
  where
    parseSingle = fromMaybe (Leaf ".") . foldr compose Nothing . split "/"
    compose word Nothing = Just $ Leaf word
    compose word (Just tree) = Just $ Branch word [tree]
    joinTrees [] = Leaf "."
    joinTrees [a] = a
    joinTrees trees = Branch "." trees

mergeTwoTrees :: Tree -> Tree -> [Tree]
mergeTwoTrees a@(Leaf nameA) b@(Leaf nameB)
    | nameA == nameB = [a]
    | otherwise = [a, b]
mergeTwoTrees a@(Leaf nameA) b@(Branch nameB _)
    | nameA == nameB = [b]
    | otherwise = [a, b]
mergeTwoTrees a@(Branch nameA _) b@(Leaf nameB)
    | nameA == nameB = [a]
    | otherwise = [a, b]
mergeTwoTrees a@(Branch nameA treeA) b@(Branch nameB treeB)
    | nameA == nameB = [Branch nameA (mergeTrees (treeA ++ treeB))]
    | otherwise = [a, b]

mergeTrees :: [Tree] -> [Tree]
mergeTrees = foldl combine []
  where
    combine [] tree = [tree]
    combine (x:xs) tree =
        case mergeTwoTrees x tree of
            [a] -> a : xs
            [a, b] -> a : combine xs tree

isLeaf :: Tree -> Bool
isLeaf (Leaf _) = True
isLeaf _ = False
