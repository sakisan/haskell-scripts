#!/usr/bin/env stack
{- stack --resolver lts-12.9 script -}
import Data.Maybe
import Data.String.Utils
import System.Environment
import System.Exit
import System.IO
import Tree

main :: IO ()
main = do
    input <- getContents
    putStrLn . showSparseTree . parseTree $ input

showTree :: Tree -> String
showTree (Leaf name) = name
showTree (Branch name trees) =
    join "\n" $ name : (fmap (prependParent . showTree) trees)
  where
    prependParent = join "\n" . fmap prependName . lines
    prependName s = name ++ "/" ++ s

showSparseTree :: Tree -> String
showSparseTree (Leaf name) = name
showSparseTree (Branch name trees) =
    join "\n" $ name : (fmap (prependParent . showSparseTree) trees)
  where
    prependParent = join "\n" . fmap prependName . lines
    prependName s = fmap (const ' ') name ++ "/" ++ s
