#!/usr/bin/env stack
{- stack --resolver lts-12.9 script -}
-- example usage:
-- ag "$@" | cut -d':' -f 1 | sort | uniq -c | awk '{print $2"("$1")"}' | collapse_leaves
import Data.Maybe
import Data.String.Utils
import System.Environment
import System.Exit
import System.IO
import Tree

main :: IO ()
main = do
    input <- getContents
    putStrLn . collapseLeaves . parseTree $ input

parentSeparator :: String
parentSeparator = "/"

verticalParentSeparator :: String
verticalParentSeparator = "  |"

siblingSeparator :: String
siblingSeparator = ", "

collapseLeaves :: Tree -> String
collapseLeaves (Leaf name) = name
collapseLeaves (Branch name [Leaf singleLeaf]) =
    name ++ parentSeparator ++ singleLeaf
-- commented out: inline when there's only one subdirectory
-- collapseLeaves (Branch name [Branch singleBranch tree]) =
--     collapseLeaves (Branch (name ++ parentSeparator ++ singleBranch) tree)
collapseLeaves (Branch name trees) =
    if all isLeaf trees
        then name ++
             parentSeparator ++
             "[" ++ (join siblingSeparator (fmap collapseLeaves trees)) ++ "]"
        else join "\n" $ name : (fmap (prependParent . collapseLeaves) trees)
  where
    prependParent = join "\n" . fmap prependName . lines
    prependName s = verticalParentSeparator ++ s
