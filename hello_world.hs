#!/usr/bin/env stack
{- stack --resolver lts-12.9 script -}

import System.Environment
import System.Exit
import System.IO

main :: IO ()
main = do
    args <- getArgs
    let putStrLnErr = hPutStrLn stderr
    if length args > 0
        then putStrLnErr (show args)
        else die "No args passed in"

    putStrLn "stdin:"
    getContents >>= putStrLn
